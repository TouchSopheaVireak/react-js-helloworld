import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Welcome to My First React JS Project
        </p>
        <h3>Touch Sopheavireak</h3>
        <a
          className="App-link"
          href="www.google.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Google.com
        </a>
      </header>
    </div>
  );
}

export default App;
